# https://www.gnu.org/software/make/manual/make.html

help:
	@echo "help       show this help"
	@echo "run        run the project and if modify the source is updated"
	@echo "dist       generate zip for production"
	@echo "clean      clean the project"

run: gradlew
	./gradlew kotlinUpgradeYarnLock
	# https://docs.gradle.org/current/userguide/command_line_interface.html#sec:continuous_build
	./gradlew -t run

dist: gradlew
	./gradlew clean kotlinUpgradeYarnLock zip

clean: gradlew
	./gradlew clean

gradlew:
	# XXX is not a prerequisite because we don't want to rebuild when is updated
	touch settings.gradle.kts
	# We want the latest version
	docker pull gradle
	# https://hub.docker.com/_/gradle/
	docker run \
		--rm \
		--user $(shell id -u):$(shell id -g) \
		--volume "${PWD}":/tmp/gradle/project \
		--workdir /tmp/gradle/project \
		gradle \
		gradle wrapper

# https://www.gnu.org/software/make/manual/html_node/Phony-Targets.html
.PHONY: help \
	run \
	dist \
	clean \
