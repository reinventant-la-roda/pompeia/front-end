plugins {
    val kotlinVersion = libs.versions.kotlin.get()
    kotlin("js") version kotlinVersion apply false
    kotlin("plugin.serialization") version kotlinVersion apply false
}

group = "org.bookstore"

subprojects {
    repositories {
        mavenCentral()
    }
}
